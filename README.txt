Modifies the core search so you can exclude node types search results.

Also enables redirections for specific node types.

For example if you have a team members view using a team member type,
you can set it so all team members that come up in the search are
redirected to the team members page. PHP filter is also available
if you have more complex cases.

Additional features includes being able to hide the default node
information from the search results, changing the default no results
text and enabling simple sorting options.
