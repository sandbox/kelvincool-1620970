<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 */
?>
<?php if ($search_results): ?>
  <?php if ($show_sorting_options): ?>
    <ul class="search-results-sort">
      <li><?php echo l(t('Most relevant'), $_GET['q']); ?></li>
      <li><?php echo l(t('Most recent'), $_GET['q'], array('query' => array('sort' => 'date', 'dir' => 'desc'))); ?></li>
    </ul>
  <?php endif; ?>
  <h2><?php print t('Search results');?></h2>
  <ol class="search-results <?php print $module; ?>-results">
    <?php print $search_results; ?>
  </ol>
  <?php print $pager; ?>
<?php else : ?>
  <?php if (is_array($no_results) && !empty($no_results['value'])): ?>
    <?php $no_results_message = check_markup($no_results['value'], $no_results['format']); ?>
    <?php print render($no_results_message); ?>
  <?php else: ?>
    <h2><?php print t('Your search yielded no results');?></h2>
    <?php print search_help('search#noresults', drupal_help_arg()); ?>
  <?php endif; ?>
<?php endif; ?>
